package com.iteaj.iot.client.http;

import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.ClientConnectProperties;
import com.iteaj.iot.client.IotClient;

import java.util.function.Consumer;

/**
 * http客户端
 * @see okhttp3.OkHttpClient
 */
public class HttpClient implements IotClient {

    @Override
    public int getPort() {
        return 0;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public ClientConnectProperties getConfig() {
        return null;
    }

    @Override
    public void init(Object arg) { }

    @Override
    public Object connect() {
        return null;
    }

    @Override
    public Object disconnect() {
        return null;
    }

    @Override
    public Object close() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ClientComponent getClientComponent() {
        return null;
    }
}
