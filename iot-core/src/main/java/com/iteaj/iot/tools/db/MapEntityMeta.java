package com.iteaj.iot.tools.db;

import java.util.List;

public interface MapEntityMeta extends DBMeta {

    @Override
    List<ParamValue> getParams(Object entity);
}
