package com.iteaj.iot.server;

import com.iteaj.iot.*;
import com.iteaj.iot.server.manager.TcpDeviceManager;
import com.iteaj.iot.server.udp.UdpDeviceManager;
import com.iteaj.iot.server.udp.UdpServerComponent;

public class DefaultDeviceManagerFactory implements DeviceManagerFactory {

    @Override
    public DeviceManager<? extends Object> createDeviceManager(ServerComponent component) {
        if(component instanceof TcpServerComponent) {
            return new TcpDeviceManager(component.getName(), IotThreadManager.instance().getDeviceManageEventExecutor());
        } else if(component instanceof UdpServerComponent) {
            return new UdpDeviceManager();
        } else {
            throw new FrameworkException("不支持的组件");
        }
    }

}
