package com.iteaj.iot.test.modbus.dtu;

import com.iteaj.iot.Message;
import com.iteaj.iot.consts.ExecStatus;
import com.iteaj.iot.format.ABCDFormat;
import com.iteaj.iot.format.DataFormat;
import com.iteaj.iot.format.DataFormatConvert;
import com.iteaj.iot.modbus.ModbusCommonProtocol;
import com.iteaj.iot.modbus.Payload;
import com.iteaj.iot.modbus.WriteConvert;
import com.iteaj.iot.modbus.consts.ModbusBitStatus;
import com.iteaj.iot.modbus.consts.ModbusCoilStatus;
import com.iteaj.iot.modbus.consts.ModbusErrCode;
import com.iteaj.iot.modbus.server.dtu.ModbusRtuForDtuCommonProtocol;
import com.iteaj.iot.modbus.server.dtu.ModbusRtuForDtuServerComponent;
import com.iteaj.iot.modbus.server.dtu.ModbusTcpForDtuCommonProtocol;
import com.iteaj.iot.modbus.server.dtu.ModbusTcpForDtuServerComponent;
import com.iteaj.iot.modbus.server.rtu.ModbusRtuBody;
import com.iteaj.iot.modbus.server.rtu.ModbusRtuHeader;
import com.iteaj.iot.modbus.server.tcp.ModbusTcpBody;
import com.iteaj.iot.modbus.server.tcp.ModbusTcpHeader;
import com.iteaj.iot.server.ServerProtocolHandle;
import com.iteaj.iot.server.TcpServerComponent;
import com.iteaj.iot.server.dtu.DtuCommonProtocolType;
import com.iteaj.iot.server.dtu.protocol.DtuDeviceSnProtocol;
import com.iteaj.iot.test.IotTestHandle;
import com.iteaj.iot.test.IotTestProperties;
import com.iteaj.iot.test.TestConst;
import com.iteaj.iot.test.plc.TestPlcUtils;
import com.iteaj.iot.utils.ByteUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.TimeUnit;

public class ModbusDtuTestHandle implements ServerProtocolHandle<DtuDeviceSnProtocol>, IotTestHandle {

    private String modbusRtuDeviceSn;
    private String modbusTcpDeviceSn;

    @Autowired(required = false)
    private ModbusTcpForDtuServerComponent component;

    @Autowired(required = false)
    private ModbusRtuForDtuServerComponent rtuForDtuComponent;

    @Autowired
    private IotTestProperties properties;
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Object handle(DtuDeviceSnProtocol deviceSnProtocol) {
        String equipCode = deviceSnProtocol.getEquipCode();
        // ascii格式
        if(equipCode.startsWith("TCP")) {
            this.modbusTcpDeviceSn = equipCode;
        }

        // 十六进制格式
        if(equipCode.startsWith("525455")) {
            this.modbusRtuDeviceSn = equipCode;
        }

        return null;
    }

    private void modbusWrite10BatchTest(ModbusCommonProtocol commonProtocol, ExecStatus status, int start
            , TcpServerComponent component, ModbusErrCode errCode, DataFormat format) {
        Message.MessageHead head = commonProtocol.requestMessage().getHead();
        if (status == ExecStatus.success) {
            if(errCode == null) {
                Payload payload = commonProtocol.getPayload(format);
                short aShort = payload.readShort(start);
                int readInt = payload.readInt(start + 1);
                float readFloat = payload.readFloat(start + 3);
                double readDouble = payload.readDouble(start + 5);
                long readLong = payload.readLong(start + 9);
                String readString = payload.readString(start + 13, 3);

                logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(args) -> Read03"
                        , head.getEquipCode(), head.getMessageId(), 760, 5 == aShort && 2 == readInt && 1.8f == readFloat
                                && 3.5 == readDouble && 300000L == readLong && "你好".equals(readString) ? "通过" : "失败", "(short)5, 2, 1.8f, 3.5, 300000L, \"你好\"");
            } else {
                logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(args) -> Read03"
                        , head.getEquipCode(), head.getMessageId(), 760, "失败("+errCode.getDesc()+")", "(short)5, 2, 1.8f, 3.5, 300000L, \"你好\"");
            }
        } else {
            logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(args) -> Read03"
                    , head.getEquipCode(), head.getMessageId(), 760, "失败", "(short)5, 2, 1.8f, 3.5, 300000L, \"你好\"");
        }
    }

    @Override
    public void start() throws Exception {
        short write06 = TestPlcUtils.randomShorts(1)[0];
        int write10 = Integer.MAX_VALUE;
        byte[] write0f = new byte[] {(byte) 210}; // 1 1 0 1 0 0 1 0
        ModbusCoilStatus write05 = ModbusCoilStatus.OFF;
        DataFormat format = DataFormat.ABCD;

        if(this.modbusRtuDeviceSn != null) {
            String equipCode = this.modbusRtuDeviceSn;
            System.out.println("-------------------------------------- Modbus Rtu For Dtu 测试("+format+") ----------------------------------------------");
            long start = System.currentTimeMillis();
            if(properties.isDtuAtStart()) {
                System.out.println("发送AT指令(Modbus Rtu)：@DTU:0000:GSN");
                ModbusRtuForDtuCommonProtocol.build(equipCode, "@DTU:0000:GSN".getBytes(), DtuCommonProtocolType.AT).request(protocol -> {
                    if(protocol.getExecStatus() == ExecStatus.success) {
                        byte[] message = protocol.responseMessage().getMessage();
                        System.out.println("AT响应：" + new String(message));
                    } else {
                        System.out.println("AT响应失败：" + protocol.getExecStatus());
                    }
                    return null;
                });
            }

            //测试读写单个线圈
            ModbusRtuForDtuCommonProtocol.buildWrite05(equipCode, 1, 5, write05).request();
            ModbusRtuForDtuCommonProtocol.buildRead01(equipCode, 1, 5, 1).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                    ModbusRtuHeader head = commonProtocol.requestMessage().getHead();

                    if(body.isSuccess()) {
                        ModbusBitStatus modbusBitStatus = commonProtocol.getPayload().readStatus(0);

                        boolean status = (write05 == ModbusCoilStatus.ON && modbusBitStatus == ModbusBitStatus.ON)
                                || (write05 == ModbusCoilStatus.OFF && modbusBitStatus == ModbusBitStatus.OFF);

                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write05 -> Read01", head.getEquipCode(), head.getMessageId(), 5
                                , status ? "通过" : "失败", write05);
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write05 -> Read01", head.getEquipCode(), head.getMessageId(), 5
                                , "失败("+body.getErrCode().getDesc()+")", write05);
                    }
                } else {
                    ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                            , "Write05 -> Read01", head.getEquipCode(), head.getMessageId(), 5, "失败", write05);
                }
            });

            // 测试读写多个线圈
            ModbusRtuForDtuCommonProtocol.buildWrite0F(equipCode, 1, 7, write0f).request();
            ModbusRtuForDtuCommonProtocol.buildRead01(equipCode, 1, 7, 8).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                    if(body.isSuccess()) {
                        Payload payload = commonProtocol.getPayload();
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write0F -> Read01", head.getEquipCode(), head.getMessageId(), 7
                                , write0f[0] == payload.getPayload()[0] ? "通过" : "失败", write0f);
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write0F -> Read01", head.getEquipCode(), head.getMessageId(), 7
                                , "失败("+body.getErrCode().getDesc()+")", write0f);
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                            , "Write0F -> Read01", head.getEquipCode(), head.getMessageId(), 7, "失败", write0f);
                }
            });

            // 写单个寄存器测试
            ModbusRtuForDtuCommonProtocol.buildWrite06(equipCode, 1, 8, ByteUtil.getBytesOfReverse(write06)).request();
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 8, 1).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                if(body.isSuccess()) {
                    if (protocol.getExecStatus() == ExecStatus.success) {
                        short i = commonProtocol.getPayload(format).readShort(8);
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write06 -> Read03", head.getEquipCode(), head.getMessageId(), 8
                                , i == write06 ? "通过" : "失败", write06);
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write06 -> Read03", head.getEquipCode(), head.getMessageId(), 8, "失败", write06);
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                            , "Write06 -> Read03", head.getEquipCode(), head.getMessageId(), 8
                            , "失败("+body.getErrCode().getDesc()+")", write06);
                }
            });

            ModbusRtuForDtuCommonProtocol.buildWrite06(equipCode, 1, 9, write06).request();
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 9, 1).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                if(body.isSuccess()) {
                    if (protocol.getExecStatus() == ExecStatus.success) {
                        short i = commonProtocol.getPayload(format).readShort(9);
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write06(arg) -> Read03", head.getEquipCode(), head.getMessageId(), 9
                                , i == write06 ? "通过" : "失败", write06);
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write06(arg) -> Read03", head.getEquipCode(), head.getMessageId(), 9, "失败", write06);
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                            , "Write06(arg) -> Read03", head.getEquipCode(), head.getMessageId(), 9, "失败("+body.getErrCode().getDesc()+")", write06);
                }
            });

            ModbusRtuForDtuCommonProtocol.buildWrite10(equipCode, 1, 31, 2, DataFormatConvert.getInstance(format).getBytes(write10)).request();
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 31, 2).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                if(body.isSuccess()) {
                    if (protocol.getExecStatus() == ExecStatus.success) {
                        int i = commonProtocol.getPayload(format).readInt(31);
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write10(byte) -> Read03", head.getEquipCode(), head.getMessageId(), 31
                                , i == write10 ? "通过" : "失败", write10);
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write10(byte) -> Read03", head.getEquipCode(), head.getMessageId(), 31, "失败", write10);
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName(), "Write10(byte) -> Read03"
                            , head.getEquipCode(), head.getMessageId(), 31, "失败("+body.getErrCode().getDesc()+")", write10);
                }
            });

            ModbusRtuForDtuCommonProtocol.buildWrite10(equipCode, 1, 39, WriteConvert.build(write10, format)).request();
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 39, 2).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                if(body.isSuccess()) {
                    if (protocol.getExecStatus() == ExecStatus.success) {
                        int i = commonProtocol.getPayload(format).readInt(39);
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write10(convert) -> Read03", head.getEquipCode(), head.getMessageId(), 39
                                , i == write10 ? "通过" : "失败", write10);
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Write10(convert) -> Read03", head.getEquipCode(), head.getMessageId(), 39, "失败", write10);
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                            , "Write10(convert) -> Read03", head.getEquipCode(), head.getMessageId(), 39, "失败("+body.getErrCode().getDesc()+")", write10);
                }
            });

            ModbusRtuForDtuCommonProtocol.buildWrite10(equipCode, 1, 860, format, (short)5, 2, 1.8f, 3.5, 300000L, "你好").request();
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 860, 16).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                modbusWrite10BatchTest(commonProtocol, protocol.getExecStatus(), 860
                        , rtuForDtuComponent, commonProtocol.responseMessage().getBody().getErrCode(), format);
            });

            // 读取错误地址测试
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 65535, 1).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                    if(body.isSuccess()) {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName(), "Read03(异常码)"
                                , head.getEquipCode(), head.getMessageId(), 65535, "失败", "-");
                    } else {
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName(), "Read03("+body.getErrCode().getDesc()+")"
                                , head.getEquipCode(), head.getMessageId(), 65535, "通过", "-");
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName(), "Read03("+protocol.getExecStatus().desc+")"
                            , head.getEquipCode(), head.getMessageId(), 65535, "失败", "-");
                }
            });

            // 读取数据长度(拆包)测试
            ModbusRtuForDtuCommonProtocol.buildRead03(equipCode, 1, 0, 123).request(protocol -> {
                ModbusRtuForDtuCommonProtocol commonProtocol = (ModbusRtuForDtuCommonProtocol) protocol;
                ModbusRtuHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusRtuBody body = commonProtocol.responseMessage().getBody();
                    if(body.isSuccess()) {
                        logger.info(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Read03(拆包测试)", head.getEquipCode(), head.getMessageId(), 123, "通过", "-");
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                                , "Read03(拆包测试)", head.getEquipCode(), head.getMessageId(), 123, "失败("+body.getErrCode().getDesc()+")", "-");
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, rtuForDtuComponent.getName()
                            , "Read03(拆包测试)", head.getEquipCode(), head.getMessageId(), 123, "失败", "-");
                }
            });

            long l = System.currentTimeMillis() - start;
            System.out.println("    总耗时：" + l + "(ms)   平均耗时：" + l / 14.0 + "(ms)");
            TimeUnit.SECONDS.sleep(1);
        }

        if(this.modbusTcpDeviceSn != null) {
            String equipCode = this.modbusTcpDeviceSn;
            System.out.println("-------------------------------------- Modbus Tcp For Dtu 测试("+format+") ----------------------------------------------");
            long start = System.currentTimeMillis();
            if(properties.isDtuAtStart()) {
                System.out.println("发送AT指令(Modbus Tcp)：@DTU:0000:ICCID");
                ModbusTcpForDtuCommonProtocol.build(equipCode, "@DTU:0000:ICCID".getBytes(), DtuCommonProtocolType.AT).request(protocol -> {
                    if(protocol.getExecStatus() == ExecStatus.success) {
                        byte[] message = protocol.responseMessage().getMessage();
                        System.out.println("AT响应：" + new String(message));
                    } else {
                        System.out.println("AT响应失败：" + protocol.getExecStatus());
                    }
                });
            }

            //测试读写单个线圈
            ModbusTcpForDtuCommonProtocol.buildWrite05(equipCode, 1, 1, write05).request();
            ModbusTcpForDtuCommonProtocol.buildRead01(equipCode, 1, 1, 1).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusBitStatus modbusBitStatus = commonProtocol.getPayload().readStatus(0);

                    boolean status = (write05 == ModbusCoilStatus.ON && modbusBitStatus == ModbusBitStatus.ON)
                            || (write05 == ModbusCoilStatus.OFF && modbusBitStatus == ModbusBitStatus.OFF);

                    ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                    logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write05 -> Read01", head.getEquipCode(), head.getMessageId(), 1
                            , status ? "通过" : "失败", write05);
                } else {
                    ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write05 -> Read01", head.getEquipCode(), head.getMessageId(), 1, "失败", write05);
                }
            });

            // 测试读写多个线圈
            ModbusTcpForDtuCommonProtocol.buildWrite0F(equipCode, 1, 3, write0f).request();
            ModbusTcpForDtuCommonProtocol.buildRead01(equipCode, 1, 3, 8).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    Payload payload = commonProtocol.getPayload();
                    logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write0F -> Read01", head.getEquipCode(), head.getMessageId(), 3
                            , write0f[0] == payload.getPayload()[0] ? "通过" : "失败", write0f);
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write0F -> Read01", head.getEquipCode(), head.getMessageId(), 3, "失败", write0f);
                }
            });

            // 写单个寄存器测试
            ModbusTcpForDtuCommonProtocol.buildWrite06(equipCode, 1, 1, ByteUtil.getBytesOfReverse(write06)).request();
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 1, 1).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    short i = commonProtocol.getPayload(format).readShort(1);
                    logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write06 -> Read03", head.getEquipCode(), head.getMessageId(), 1
                            , i == write06 ? "通过" : "失败", write06);
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write06 -> Read03", head.getEquipCode(), head.getMessageId(), 1, "失败", write06);
                }
            });

            ModbusTcpForDtuCommonProtocol.buildWrite06(equipCode, 1, 2, write06).request();
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 2, 1).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    short i = commonProtocol.getPayload(format).readShort(2);
                    logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write06(arg) -> Read03", head.getEquipCode(), head.getMessageId(), 2
                            , i == write06 ? "通过" : "失败", write06);
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName()
                            , "Write06(arg) -> Read03", head.getEquipCode(), head.getMessageId(), 2, "失败", write06);
                }
            });

            // 写多个寄存器测试
            ModbusTcpForDtuCommonProtocol.buildWrite10(equipCode, 1, 31, 2, DataFormatConvert.getInstance(format).getBytes(write10)).request();
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 31, 2).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    int i = commonProtocol.getPayload(format).readInt(31);
                    logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(byte) -> Read03"
                            , head.getEquipCode(), head.getMessageId(), 31, i == write10 ? "通过" : "失败", write10);
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(byte) -> Read03"
                            , head.getEquipCode(), head.getMessageId(), 31, "失败", write10);
                }
            });

            // 写多个寄存器测试
            ModbusTcpForDtuCommonProtocol.buildWrite10(equipCode, 1, 39, WriteConvert.build(write10, format)).request();
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 39, 2).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    int i = commonProtocol.getPayload(format).readInt(39);
                    logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(convert) -> Read03"
                            , head.getEquipCode(), head.getMessageId(), 39, i == write10 ? "通过" : "失败", write10);
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Write10(convert) -> Read03"
                            , head.getEquipCode(), head.getMessageId(), 39, "失败", write10);
                }
            });

            // 批量读写测试
            ModbusTcpForDtuCommonProtocol.buildWrite10(equipCode, 1, 760, format, (short)5, 2, 1.8f, 3.5, 300000L, "你好").request();
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 760, 16).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                modbusWrite10BatchTest(commonProtocol, protocol.getExecStatus(), 760, component
                        , commonProtocol.responseMessage().getBody().getErrCode(), format);
            });

            // 异常码测试
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 65535, 1).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusTcpBody body = commonProtocol.responseMessage().getBody();
                    if(body.isSuccess()) {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Read03(异常码)"
                                , head.getEquipCode(), head.getMessageId(), 65535, "失败", "-");
                    } else {
                        logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Read03("+body.getErrCode().getDesc()+")"
                                , head.getEquipCode(), head.getMessageId(), 65535, "通过", "-");
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Read03("+protocol.getExecStatus().desc+")"
                            , head.getEquipCode(), head.getMessageId(), 65535, "失败", "-");
                }
            });

            // 读取数据长度(拆包)测试
            ModbusTcpForDtuCommonProtocol.buildRead03(equipCode, 1, 0, 125).request(protocol -> {
                ModbusTcpForDtuCommonProtocol commonProtocol = (ModbusTcpForDtuCommonProtocol) protocol;
                ModbusTcpHeader head = commonProtocol.requestMessage().getHead();
                if (protocol.getExecStatus() == ExecStatus.success) {
                    ModbusTcpBody body = commonProtocol.responseMessage().getBody();
                    if(body.isSuccess()) {
                        logger.info(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Read03(拆包测试)"
                                , head.getEquipCode(), head.getMessageId(), 0, "通过", "-");
                    } else {
                        logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Read03(拆包测试)"
                                , head.getEquipCode(), head.getMessageId(), 0, "失败("+body.getErrCode().getDesc()+")", "-");
                    }
                } else {
                    logger.error(TestConst.LOGGER_MODBUS_DESC, component.getName(), "Read03(拆包测试)"
                            , head.getEquipCode(), head.getMessageId(), 0, "失败", "-");
                }
            });

            long l = System.currentTimeMillis() - start;
            System.out.println("    总耗时：" + l + "(ms)   平均耗时：" + l / 14.0 + "(ms)");
            TimeUnit.SECONDS.sleep(1);
        }

    }

    @Override
    public int getOrder() {
        return 1200;
    }
}
