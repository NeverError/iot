package com.iteaj.iot.simulator;

import com.iteaj.iot.ProtocolType;

public enum SimulatorProtocolType implements ProtocolType {
    DTU("DTU模拟器");

    private String desc;

    SimulatorProtocolType(String desc) {
        this.desc = desc;
    }

    @Override
    public Enum getType() {
        return this;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }
}
